/* FILE NAME: T04GLOBE.C
 * PROGRAMMER: VK1
 * DATE: 04.06.2015
 * PURPOSE: Drawing globus.
 */

#pragma warning(disable: 4244)

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <windows.h>

#define PI 3.1415926

/* ��� ������ ���� */
#define WND_CLASS_NAME "My window class"
#define MAXN 501
#define MAXM 501

/* ��� �������� ��������� ����� � ������������ */
typedef struct
{
  DOUBLE X, Y, Z;
} VEC;

typedef struct
{
  HBITMAP hBm;
  HDC hDC;
  INT H, W;
} IMAGE;

IMAGE globe;

VEC SPHERE[MAXN][MAXM];

/* ������ ������ */
LRESULT CALLBACK MyWindowFunc( HWND hWnd, UINT Msg,
                               WPARAM wParam, LPARAM lParam );

/* ������� ������� ���������.
 *   - ���������� ���������� ����������:
 *       HINSTANCE hInstance;
 *   - ���������� ����������� ���������� ����������
 *     (�� ������������ � ������ ���� NULL):
 *       HINSTANCE hPrevInstance;
 *   - ��������� ������:
 *       CHAR *CmdLine;
 * ������������ ��������:
 *   (INT) ��� �������� � ������������ �������.
 *   0 - ��� ������.
 */
INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    CHAR *CmdLine, INT ShowCmd )
{
  WNDCLASS wc;
  HWND hWnd;
  MSG msg;
  /* HINSTANCE hIns = LoadLibrary("shell32"); */

  /* ����������� ������ ���� */
  wc.style = CS_VREDRAW | CS_HREDRAW; /* ����� ����: ��������� ��������������
                                       * ��� ��������� ������������� ���
                                       * ��������������� ��������
                                       * ��� ����� CS_DBLCLKS ��� ����������
                                       * ��������� �������� ������� */
  wc.cbClsExtra = 0; /* �������������� ���������� ���� ��� ������ */
  wc.cbWndExtra = 0; /* �������������� ���������� ���� ��� ���� */
  wc.hbrBackground = CreateSolidBrush(RGB(255, 255, 0));
  wc.hCursor = LoadCursor(NULL, IDC_HAND); /* �������� ������� (����������) */
  wc.hIcon = LoadIcon(NULL, IDI_ASTERISK); /* �������� ����������� (���������) */
  wc.hInstance = hInstance; /* ���������� ����������, ��������������� ����� */
  wc.lpszMenuName = NULL; /* ��� ������� ���� */
  wc.lpfnWndProc = MyWindowFunc; /* ��������� �� ������� ��������� */
  wc.lpszClassName = WND_CLASS_NAME;

  /* ����������� ������ � ������� */
  if (!RegisterClass(&wc))
  {
    MessageBox(NULL, "Error register window class", "ERROR", MB_OK);
    return 0;
  }

  /* �������� ���� */
  hWnd =
    CreateWindow(WND_CLASS_NAME,    /* ��� ������ ���� */
      "Clock",                      /* ��������� ���� */
      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,          /* ����� ���� - ���� ������ ���� */
      CW_USEDEFAULT, CW_USEDEFAULT, /* ������� ���� (x, y) - �� ��������� */
      400, 520,                     /* ������� ���� (w, h) - �� ��������� */
      NULL,                         /* ���������� ������������� ���� */
      NULL,                         /* ���������� ������������ ���� */
      hInstance,                    /* ���������� ���������� */
      NULL);                        /* ��������� �� �������������� ��������� */

  ShowWindow(hWnd, ShowCmd);
  UpdateWindow(hWnd);

  /* ������ ����� ��������� ���� */
  while (GetMessage(&msg, NULL, 0, 0))
  {
    /* ��������� ��������� �� ���������� */
    TranslateMessage(&msg);
    /* �������� ��������� � ������� ���� */
    DispatchMessage(&msg);
  }

  return msg.wParam;
} /* End of 'WinMain' function */



/* Load image function.
 * ARGUMENTS:
 *   - image store data:
 *       IMAGE *Img;
 *   - image file name:
 *       CHAR *FileName;
 * RETURNS:
 *   (BOOL) TRUE if success, FALSE otherwise.
 */
BOOL ImageLoad( IMAGE *Img, CHAR *FileName )
{
  HDC hDC;
  BITMAP bm;

  memset(Img, 0, sizeof(IMAGE));

  /* Load image from file */
  Img->hBm = LoadImage(NULL, FileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
  if (Img->hBm == NULL)
    return FALSE;

  /* Create compatible context and select image into one */
  hDC = GetDC(NULL);
  Img->hDC = CreateCompatibleDC(hDC);
  ReleaseDC(NULL, hDC);

  SelectObject(Img->hDC, Img->hBm);

  /* Obtain image size */
  GetObject(Img->hBm, sizeof(BITMAP), &bm);
  Img->W = bm.bmWidth;
  Img->H = bm.bmHeight;
  return TRUE;
} /* End of 'ImageLoad' function */

/* Free memory from image function.
 * ARGUMENTS:
 *   - image store data:
 *       IMAGE *Img;
 * RETURNS: None.
 */
VOID ImageFree( IMAGE *Img )
{
  if (Img->hBm != NULL)
    DeleteObject(Img->hBm);
  if (Img->hDC != NULL)
    DeleteDC(Img->hDC);
  memset(Img, 0, sizeof(IMAGE));
} /* End of 'ImageFree' function */

/* Obtain image pixel color function.
 * ARGUMENTS:
 *   - image store data:
 *       IMAGE *Img;
 *   - pixel coordinates:
 *       INT X, Y;
 * RETURNS:
 *   (DWORD) TRUE if success, FALSE otherwise.
 */
DWORD ImageGetP( IMAGE *Img, INT X, INT Y )
{
  if (Img->hBm == NULL)
    return 0;

  return GetPixel(Img->hDC, X, Y);
} /* End of 'ImageGetP' function */



/* ������� �������� ����� ������ ��� X */
VEC RotateX( VEC P, DOUBLE AngleDegree )
{
  DOUBLE a = AngleDegree * PI / 180, si = sin(a), co = cos(a);
  VEC r;

  r.X = P.X;
  r.Y = P.Y * co - P.Z * si;
  r.Z = P.Y * si + P.Z * co;
  return r;
} /* End of 'RotateX' function */

/* ������� ���������� ������������ */
VEC VecCrossVec( VEC A, VEC B )
{
  VEC r;

  r.X = A.Y * B.Z - A.Z * B.Y;
  r.Y = A.Z * B.X - A.X * B.Z;
  r.Z = A.X * B.Y - A.Y * B.X;
  return r;
} /* End of 'VecCrossVec' function */

/* ������� ��������� �������� */
VEC VecSubVec( VEC A, VEC B )
{
  VEC r;

  r.X = A.X - B.X;
  r.Y = A.Y - B.Y;
  r.Z = A.Z - B.Z;
  return r;
} /* End of 'VecSubVec' function */

VOID BuildSphere(INT N, INT M, INT R)
{
  INT i, j;
  DOUBLE fi, tet, Angle = (DOUBLE)clock() / CLOCKS_PER_SEC / 5;
  for (i = 0; i <= N; i++)
  {

    fi = 2 * PI / N * i + Angle;
    for (j = 0; j <= M; j++)
    {
      tet = PI / M * j;
      SPHERE[i][j].X = R * sin(fi) * sin(tet);
      SPHERE[i][j].Y = 0.888 * R * cos(tet);
      SPHERE[i][j].Z = R * sin(tet) * cos(fi);
      SPHERE[i][j] = RotateX(SPHERE[i][j], 90);
    }
  }
}

VOID DrawQuad( HDC hDC, VEC P0, VEC P1, VEC P2, VEC P3, INT X, INT Y )
{
  VEC Norm = VecCrossVec(VecSubVec(P3, P0), VecSubVec(P1, P0));
  POINT pnts[4];

  if (P3.X == P0.X && P3.Y == P0.Y && P3.Z == P0.Z)
    Norm = VecCrossVec(VecSubVec(P2, P1), VecSubVec(P2, P3));
  /* back-face culling */
  if (Norm.Z < 0)
    return;
  
  SelectObject(hDC, GetStockObject(NULL_PEN));
  SelectObject(hDC, GetStockObject(DC_BRUSH));

  pnts[0].x = P0.X + X;
  pnts[0].y = -P0.Y + Y;

  pnts[1].x = P1.X + X;
  pnts[1].y = -P1.Y + Y;

  pnts[2].x = P2.X + X;
  pnts[2].y = -P2.Y + Y;

  pnts[3].x = P3.X + X;
  pnts[3].y = -P3.Y + Y;

  Polygon(hDC, pnts, 4);
}

VOID DrawSphere(HDC hMemDC, INT N, INT M, INT X, INT Y)
{
  INT i, j;
  srand(30);
  for (i = 0; i < N; i++)
    for (j = 0; j <= M; j++)   
    {
      SetDCBrushColor(hMemDC, ImageGetP(&globe, i * globe.W / N, j * globe.H / M));
      DrawQuad(hMemDC, SPHERE[i][j], SPHERE[i][j + 1], SPHERE[i + 1][j + 1], SPHERE[i + 1][j], X, Y);
    }
}

/* ������� ��������� �����.
 * ���������:
 *   - ���������� ��������� ���������:
 *       HDC hMemDC;
 *   - ������� ������ �����:
 *       INT �, �;
 *   - ������ �����:
 *       INT r;         
 */ 

VOID Sphere(HDC hMemDC, INT x, INT y, INT r)
{
  VEC a, b, c, d, Norm;
  DOUBLE fi, tet, Angle = (DOUBLE)clock() / CLOCKS_PER_SEC / 5;
  POINT pnts[4];
  INT N = 30, M = 30, i, j;
  DOUBLE stepF = 2 * PI / N, stepT = PI / M;
  SelectObject(hMemDC, GetStockObject(DC_PEN));
  SelectObject(hMemDC, GetStockObject(DC_BRUSH));
  srand(30);
  for (i = 0; i < N; i++)
  {
    fi = 2 * PI / N * i + Angle;
    for (j = 0; j < M; j++)
    {
      tet = PI / M * j;
      SetDCBrushColor(hMemDC, RGB(rand() % 256, rand() % 256, rand() % 256));
      a.X = pnts[0].x = x + r * sin(fi) * sin(tet);
      a.Y = pnts[0].y = y + r * cos(tet);
      a.Z = r * sin(tet) * cos(fi);
      b.X = pnts[1].x = x + r * sin(fi) * sin(tet + stepT);
      b.Y = pnts[1].y = y + r * cos(tet + stepT);
      b.Z = r * sin(tet + stepT) * cos(fi);
      c.X = pnts[2].x = x + r * sin(fi + stepF) * sin(tet + stepT);
      c.Y = pnts[2].y = y + r * cos(tet + stepT);
      c.Z = r * sin(tet + stepT) * cos(fi + stepF);
      d.X = pnts[3].x = x + r * sin(fi + stepF) * sin(tet);
      d.Y = pnts[3].y = y + r * cos(tet);
      d.Z = r * sin(tet) * cos(fi + stepF);
      if (j < N - 1)
        Norm = VecCrossVec(VecSubVec(c, b), VecSubVec(a, b));
      else
        Norm = VecCrossVec(VecSubVec(d, c), VecSubVec(a, b));
      if (Norm.Z >= 0)
        Polygon(hMemDC, pnts, 4);
    }
  }
}




/* ������� ��������� ��������� ����.
 * ���������:
 *   - ���������� ����:
 *       HWND hWnd;
 *   - ����� ��������� (��. WM_***):
 *       UINT Msg;
 *   - �������� ��������� ('word parameter'):
 *       WPARAM wParam;
 *   - �������� ��������� ('long parameter'):
 *       LPARAM lParam;
 * ������������ ��������:
 *   (LRESULT) - � ����������� �� ���������.
 */
LRESULT CALLBACK MyWindowFunc( HWND hWnd, UINT Msg,
                               WPARAM wParam, LPARAM lParam )
{
  HDC hDC;
  CREATESTRUCT *cs;
  POINT pt;
  SYSTEMTIME st;
  static HBITMAP hBm;
  static HDC hMemDC;
  static INT w, h;
  static DOUBLE ang = 0;
  switch (Msg)
  {
  case WM_CREATE:
    ImageLoad(&globe, "globe2.bmp");
    cs = (CREATESTRUCT *)lParam;
    SetTimer(hWnd, 1, 50, NULL);

    /* ������� �������� � ������ */
    hDC = GetDC(hWnd);
    hMemDC = CreateCompatibleDC(hDC);
    ReleaseDC(hWnd, hDC);
    return 0;

  case WM_SIZE:
    w = LOWORD(lParam);
    h = HIWORD(lParam);

    /* ������� �������� �������� � ���� */
    if (hBm != NULL)
      DeleteObject(hBm);

    hDC = GetDC(hWnd);
    hBm = CreateCompatibleBitmap(hDC, w, h);
    ReleaseDC(hWnd, hDC);

    SelectObject(hMemDC, hBm);
    SendMessage(hWnd, WM_TIMER, 1, 0);
    return 0;

  case WM_TIMER:
    /* Clear Background */
    SelectObject(hMemDC, GetStockObject(NULL_PEN));
    SelectObject(hMemDC, GetStockObject(DC_BRUSH));
    SetDCBrushColor(hMemDC, RGB(255, 255, 255));
    Rectangle(hMemDC, 0, 0, w + 1, h + 1);

    /* Draw picture */  
    //Sphere(hMemDC, w / 2, h / 2, min(w, h) / 2);
    BuildSphere(100, 50, min(w, h) / 2);
    DrawSphere(hMemDC, 100, 50, w / 2, h / 2);
    InvalidateRect(hWnd, NULL, TRUE);
    return 0;

  case WM_ERASEBKGND:
    BitBlt((HDC)wParam, 0, 0, w, h, hMemDC, 0, 0, SRCCOPY);
    return 0;

  case WM_KEYDOWN:
	if (wParam == VK_ESCAPE)
	  SendMessage(hWnd, WM_DESTROY, 0, 0);
	return 0;

  case WM_DESTROY:
    ImageFree(&globe);
    DeleteDC(hMemDC);
    DeleteObject(hBm);
    KillTimer(hWnd, 1);
    PostQuitMessage(0);
    return 0;
  }
  return DefWindowProc(hWnd, Msg, wParam, lParam);
} /* End of 'MyWindowFunc' function */

/* END OF 'T02CLOCK.C' FILE */
