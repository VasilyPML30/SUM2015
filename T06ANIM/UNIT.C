/* FILENAME: UNIT.C
 * PROGRAMMER: VK1
 * PURPOSE: Animation unit handle module.
 * LAST UPDATE: 08.06.2015
 */

#include <string.h>

#include "anim.h"

/* ������� ��-��������� ������������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       VK1UNIT *Uni;
 *   - ��������� �� �������� ��������:
 *       VK1ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VK1_AnimUnitInit( VK1UNIT *Uni, VK1ANIM *Ani )
{
} /* End of 'VK1_AnimUnitInit' function */

/* ������� ��-��������� ��������������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       VK1UNIT *Uni;
 *   - ��������� �� �������� ��������:
 *       VK1ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VK1_AnimUnitClose( VK1UNIT *Uni, VK1ANIM *Ani )
{
} /* End of 'VK1_AnimUnitClose' function */

/* ������� ��-��������� ���������� ����������� ���������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       VK1UNIT *Uni;
 *   - ��������� �� �������� ��������:
 *       VK1ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VK1_AnimUnitResponse( VK1UNIT *Uni, VK1ANIM *Ani )
{
} /* End of 'VK1_AnimUnitResponse' function */

/* ������� ��-��������� ���������� ������� ��������.
 * ���������:
 *   - ��������� �� "����" - ��� ������ ��������:
 *       VK1UNIT *Uni;
 *   - ��������� �� �������� ��������:
 *       VK1ANIM *Ani;
 * ������������ ��������: ���.
 */
static VOID VK1_AnimUnitRender( VK1UNIT *Uni, VK1ANIM *Ani )
{
} /* End of 'VK1_AnimUnitRender' function */

/* ������� �������� ������� ��������.
 * ���������:
 *   - ������ ��������� ������� ��������:
 *       INT Size;
 * ������������ ��������:
 *   (VK1UNIT *) ��������� �� ��������� ������ ��������.
 */
VK1UNIT * VK1_AnimUnitCreate( INT Size )
{
  VK1UNIT *Uni;

  if (Size < sizeof(VK1UNIT) || (Uni = malloc(Size)) == NULL)
    return NULL;
  memset(Uni, 0, Size);
  /* ��������� ���� ��-��������� */
  Uni->Size = Size;
  Uni->Init = VK1_AnimUnitInit;
  Uni->Close = VK1_AnimUnitClose;
  Uni->Response = VK1_AnimUnitResponse;
  Uni->Render = VK1_AnimUnitRender;
  return Uni;
} /* End of 'VK1_AnimUnitCreate' function */

/* END OF 'UNIT.C' FILE */
