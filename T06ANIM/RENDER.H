/* FILENAME: RENDER.H
 * PROGRAMMER: VK1
 * PURPOSE: Rendering system declaration module.
 * LAST UPDATE: 09.06.2015
 */

#ifndef __RENDER_H_
#define __RENDER_H_

#include "vec.h"

/* ��� ������������� ������� - ����� ������ � ������������� */
typedef struct tagVK1GOBJ
{
  VEC *V;      /* ������ ������ ������� */
  INT NumOfV;  /* ���������� ������ */
  INT (*F)[3]; /* ������ ������ ������� */
  INT NumOfF;  /* ���������� ������ */
} VK1GOBJ;

/* ������� */
extern MATR
  VK1_RndMatrWorld,
  VK1_RndMatrView,    
  VK1_RndMatrFrustum;

/* ��������� ������������� */
extern DBL
  VK1_RndWp, VK1_RndHp,    /* ������� ������� ������������� */
  VK1_RndProjDist;         /* ���������� �� ��������� �������� */

/* ������� �������������� �� ������� ������� ��������� � ����.
 * ���������:
 *   - �������� �����:
 *       VEC P;
 * ������������ ��������:
 *   (POINT) ���������� � �����.
 */
POINT VK1_RndWorldToScreen( VEC P, VK1ANIM *Ani );

/* ������� �������� ��������������� �������.
 * ���������:
 *   - ��������� ������� ��� ��������:
 *       VK1GOBJ *GObj;
 *   - ��� �����:
 *       CHAR *FileName;
 * ������������ ��������:
 *   (BOOL) TRUE ��� ������, FALSE �����.
 */
BOOL VK1_RndGObjLoad( VK1GOBJ *GObj, CHAR *FileName );

/* ������� ������������ ������ ��-��� ��������������� �������.
 * ���������:
 *   - ��������� ������� ��� ��������:
 *       VK1GOBJ *GObj;
 * ������������ ��������: ���.
 */
VOID VK1_RndGObjFree( VK1GOBJ *GObj );

/* ������� ��������� ��������������� �������.
 * ���������:
 *   - ��������� ������� ��� ��������:
 *       VK1GOBJ *GObj;
 * ������������ ��������: ���.
 */
VOID VK1_RndGObjDraw( VK1GOBJ *GObj, VK1ANIM *Ani);

#endif /* __RENDER_H_ */

/* END OF 'RENDER.H' FILE */
