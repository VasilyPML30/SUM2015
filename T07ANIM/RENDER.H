/* FILENAME: RENDER.H
 * PROGRAMMER: VK1
 * PURPOSE: Rendering system declaration module.
 * LAST UPDATE: 13.06.2015
 */

#ifndef __RENDER_H_
#define __RENDER_H_

#include "vec.h"

#define GLEW_STATIC
#include <glew.h>
#include <gl/gl.h>
#include <gl/glu.h>

/* ������� */
extern MATR
  VK1_RndMatrWorld,
  VK1_RndMatrView,
  VK1_RndMatrProj,
  VK1_RndMatrWorldViewProj;

/* ������� ��������� ��������� ��� �������� */
extern MATR VK1_RndPrimMatrConvert;

/* ��������� ������������� */
extern DBL
  VK1_RndWp, VK1_RndHp,    /* ������� ������� ������������� */
  VK1_RndProjDist;         /* ���������� �� ��������� �������� */

/* ������ �� ��������� */
extern UINT VK1_RndProg;

/* ������� �������� �������� ��� ����� ���������.
 * ���������:
 *   - ������� ����� �����:
 *       CHAR *FileNamePrefix;
 * ������������ ��������:
 *   (UINT) ����� ����������� ���������.
 */
UINT VK1_ShaderLoad( CHAR *FileNamePrefix );

/* ������� ����������� �������� ��� ���������.
 * ���������:
 *   - ����� ���������:
 *       UINT PrgId;
 * ������������ ��������: ���.
 */
VOID VK1_ShaderFree( UINT PrgId );

/***
 * ������ � �����������
 ***/

/* ��� ������� ��������� ����������� */
typedef struct tagVK1MATERIAL
{
  /* ��� ��������� */
  CHAR Name[300];

  /* ������������ ��������� */
  VEC Ka, Kd, Ks;
  FLT Kp; /* ������� "����������" */

  /* ����������� ������������ */
  FLT Kt;

  /* �������� ����������� */
  INT TexId;
} VK1MATERIAL;

/* ���������� ������� ���������� */
#define VK1_MATERIAL_MAX 300
extern INT VK1_MtlLibSize;
extern VK1MATERIAL VK1_MtlLib[VK1_MATERIAL_MAX];

/* ������� ���������� ��������� � ����������.
 * ���������:
 *   - ��������� � ����������� ���������:
 *       VK1MATERIAL *Mtl;
 * ������������ ��������:
 *   (INT) ���������� ����� ������������ ��������� (0 ��� ������).
 */
INT VK1_MtlAdd( VK1MATERIAL *Mtl );

/* ������� ������ ��������� �� �����.
 * ���������:
 *   - ��� ���������:
 *       CHAR *Name;
 * ������������ ��������:
 *   (INT) ���������� ����� ���������� ��������� (0 ��� ������).
 */
INT VK1_MtlFind( CHAR *Name );

/* ������� �������� ��������� �� ����� (*.MTL).
 * ���������:
 *   - ��� ����� ���������:
 *       CHAR *FileName;
 * ������������ ��������:
 *   (INT) ���������� ����������� ����������.
 */
INT VK1_MtlLoad( CHAR *FileName );

/***
 * ������ � �����������
 ***/

/* ���� ���������� */
typedef enum tagVK1PRIM_TYPE
{
  VK1_PRIM_TRIMESH,  /* �������� - ����� ������������� */
  VK1_PRIM_GRID      /* ���������� ����� ������������� (����� triangle strip) */
} VK1PRIM_TYPE;

/* ��� �������� ��������� */
typedef struct tagVK1PRIM
{
  VK1PRIM_TYPE Type; /* ��� ��������� (VK1_PRIM_***) */
  INT
    VA,          /* ������ ������ */
    VBuf, IBuf,  /* ������ ������ � �������� */
    NumOfI,      /* ���������� �������� ��� ������ ��������� */
    MtlNo;       /* ����� ��������� �� ���������� */
  FLT Shift;
} VK1PRIM;

/* ��� �������� ���������� ��������� */
typedef struct tagVK1UV
{
  FLT U, V; /* ���������� ���������� */
} VK1UV;

/* ������� ������� ���������� ���������.
 * ���������:
 *   - ����� ������� ���������:
 *       FLT U, V;
 * ������������ ��������:
 *   (VK1UV) ����������� ����������.
 */
__inline VK1UV UVSet( FLT U, FLT V )
{
  VK1UV UV = {U, V};

  return UV;
} /* End of 'UVSet' function */

/* ��� �������� ����� ������� ���������� */
typedef struct tagVK1VERTEX
{
  VEC P;    /* ������� ������� */
  VK1UV T;  /* ���������� ���������� ������� */
  VEC N;    /* ������� � ������� */
  COLOR C;  /* ���� ������� */
} VK1VERTEX;

/* ������� �������� ���������.
 * ���������:
 *   - ��������� �� ��������:
 *       VK1PRIM *Prim;
 *   - ��� ��������� (VK1_PRIM_***):
 *       VK1PRIM_TYPE Type;
 *   - ���������� ������ � ��������:
 *       INT NoofV, NoofI;
 *   - ������ ������:
 *       VK1VERTEX *Vertices;
 *   - ������ ��������:
 *       INT *Indices;
 * ������������ ��������: ���.
 */
VOID VK1_PrimCreate( VK1PRIM *Prim, VK1PRIM_TYPE Type,
                     INT NoofV, INT NoofI, VK1VERTEX *Vertices, INT *Indices);

/* ������� �������� ���������.
 * ���������:
 *   - ��������� �� ��������:
 *       VK1PRIM *Prim;
 * ������������ ��������: ���.
 */
VOID VK1_PrimFree( VK1PRIM *Prim );

/* ������� ��������� ���������.
 * ���������:
 *   - ��������� �� ��������:
 *       VK1PRIM *Prim;
 * ������������ ��������: ���.
 */
VOID VK1_PrimDraw( VK1PRIM *Prim );

/* ������� �������� ��������� ���������.
 * ���������:
 *   - ��������� �� ��������:
 *       VK1PRIM *Prim;
 *   - ����������� �������-�������:
 *       VEC Du, Dv;
 *   - ���������:
 *       INT N, M;
 * ������������ ��������:
 *   (BOOL) TRUE ��� ������, ����� FALSE.
 */
BOOL VK1_PrimCreatePlane( VK1PRIM *Prim, VEC Du, VEC Dv, INT N, INT M );

/* ������� �������� ��������� �����.
 * ���������:
 *   - ��������� �� ��������:
 *       VK1PRIM *Prim;
 *   - ����� �����:
 *       VEC �;
 *   - ������ �����:
 *       FLT R;
 *   - ���������:
 *       INT N, M;
 * ������������ ��������:
 *   (BOOL) TRUE ��� ������, ����� FALSE.
 */
BOOL VK1_PrimCreateSphere( VK1PRIM *Prim, VEC C, FLT R, INT N, INT M );

/* ������� �������� ��������������� �������.
 * ���������:
 *   - ��������� ������� ��� ��������:
 *       VK1PRIM *GObj;
 *   - ��� �����:
 *       CHAR *FileName;
 * ������������ ��������:
 *   (BOOL) TRUE ��� ������, FALSE �����.
 */
BOOL VK1_PrimLoad( VK1PRIM *GObj, CHAR *FileName );

/***
 * ������ � ��������������� ���������
 ***/

/* �������������� ������ - ����� ���������� */
typedef struct tagVK1GEOM
{
  INT NumOfPrimitives; /* ���������� ���������� */
  VK1PRIM *Prims;      /* ������ ���������� */
} VK1GEOM;

/* ������� ���������� ��������� � ��������������� �������.
 * ���������:
 *   - ��������� �� �������������� ������:
 *       VK1GEOM *G;
 *   - ��������� �� ����������� ��������:
 *       VK1PRIM *Prim;
 * ������������ ��������:
 *   (INT) ����� ������������ ��������� � ������� (-1 ��� ������).
 */
INT VK1_GeomAddPrim( VK1GEOM *G, VK1PRIM *Prim );

/* ������� ������������ ��������������� �������.
 * ���������:
 *   - ��������� �� �������������� ������:
 *       VK1GEOM *G;
 * ������������ ��������: ���.
 */
VOID VK1_GeomFree( VK1GEOM *G );

/* ������� ����������� ��������������� �������.
 * ���������:
 *   - ��������� �� �������������� ������:
 *       VK1GEOM *G;
 * ������������ ��������: ���.
 */
VOID VK1_GeomDraw( VK1GEOM *G );

/* ������� �������� ��������������� ������� �� G3D �����.
 * ���������:
 *   - ��������� �� �������������� ������:
 *       VK1GEOM *G;
 *   - ��� �����:
 *       CHAR *FileName;
 * ������������ ��������:
 *   (BOOL) TRUE ��� ������, ����� - FALSE.
 */
BOOL VK1_GeomLoad( VK1GEOM *G, CHAR *FileName );

/* ������� �������� ��������.
 * ���������:
 *   - ��� �����:
 *       CHAR *FileName;
 * ������������ ��������:
 *   (INT ) ������������� OpenGL ��� ��������.
 */
INT VK1_TextureLoad( CHAR *FileName );

#endif /* __RENDER_H_ */

/* END OF 'RENDER.H' FILE */
